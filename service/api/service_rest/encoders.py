from common.json import ModelEncoder
from .models import Technician, Appointment, Status


class TechnicianListEncoder(ModelEncoder):
    model = Technician
    properties = ['first_name', 'last_name', 'employee_id', 'id']


class TechnicianDetailEncoder(ModelEncoder):
    model = Technician
    properties = ['first_name', 'last_name', 'employee_id', 'id']
    

class AppointmentListEncoder(ModelEncoder):
    model = Appointment
    properties = ['date_time', 'reason', 'vin', 'customer', 'id']

    def get_extra_data(self, o):
        return {"status": o.status.name,
                "technician": o.technician.employee_id
                }


class AppointmentDetailEncoder(ModelEncoder):
    model = Appointment
    properties = ['date_time', 'reason', 'vin', 'customer', 'technician']
    encoders = {'technician': TechnicianListEncoder()}

    def get_extra_data(self, o):
        return {"status": o.status.name}


class StatusEncoder(ModelEncoder):
    model = Status
    properties = ['status_name']