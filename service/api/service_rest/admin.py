from django.contrib import admin
from .models import Technician, AutomobileVO, Appointment, Status
# Register your models here.

admin.site.register(Technician)
admin.site.register(AutomobileVO)
admin.site.register(Appointment)
admin.site.register(Status)