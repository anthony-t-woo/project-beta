from django.shortcuts import render
from django.http import JsonResponse
from django.views.decorators.http import require_http_methods
from .models import Salesperson, Customer, Sale, AutomobileVO
from .encoders import (
    SalespersonListEncoder,
    CustomerListEncoder,
    SaleEncoder
    )
import json


# Create your views here.

@require_http_methods(['GET', 'POST'])
def api_list_salespeople(request):
    """
    List salespeople.

    Returns a dictionary with key "salespeople" and a value that
    is a list, containing details for each salesperson.
    Each entry in the list is a dictionary that contains the details:
    first name, last name, and employee ID.

    {
        "salespeople": [
            {
                "last_name": last name,
                "first_name": first name,
                "employee_id": employee ID,
                "id": id
            }
        ]
    }
    """
    if request.method == 'GET':
        try:
            salespeople = Salesperson.objects.all()
            return JsonResponse(
                {"salespeople": salespeople},
                encoder=SalespersonListEncoder,
            )
        except Salesperson.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid object request"},
                status=400,
            )

    else:  # POST
        try:
            content = json.loads(request.body)
            salesperson = Salesperson.create(**content)
            return JsonResponse(
                salesperson,
                encoder=SalespersonListEncoder,
                safe=False,
            )
        except Salesperson.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid request"},
                status=400,

            )


@require_http_methods(['DELETE'])
def api_show_salesperson(request, id):
    """
    Deletes a salesperson, identified by their id.

    """
    if request.method == 'DELETE':
        try:
            count, _ = Salesperson.objects.get(id=id).delete()
            return JsonResponse({"deleted": count > 0})

        except Salesperson.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid salesperson ID"},
                status=400,
            )


@require_http_methods(['GET', 'POST'])
def api_list_customers(request):
    """
    Lists customers.

    Returns a dictionary with key "customers" and a value that
    is a list, containing details for each customer.
    Each entry in the list is a dictionary that contains the details:
    first name, last name, address, phone number, and object id.

    {
        "customers": [
            {
                "last_name": last name,
                "first_name": first name,
                "address": address,
                "phone_number": phone number,
                "id": id
            }
        ]
    }
    """
    if request.method == 'GET':
        try:
            customers = Customer.objects.all()
            return JsonResponse(
                {"customers": customers},
                encoder=CustomerListEncoder,
            )
        except Customer.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid object request"},
                status=400,
            )

    else:  # POST
        try:
            content = json.loads(request.body)
            customer = Customer.create(**content)
            return JsonResponse(
                customer,
                encoder=CustomerListEncoder,
                safe=False,
            )
        except Customer.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid request"},
                status=400,
            )

@require_http_methods(['DELETE'])
def api_show_customer(request, id):
    """
    Deletes a customer, identified by their object id.
    """
    if request.method == 'DELETE':
        try:
            count, _ = Customer.objects.get(id=id).delete()
            return JsonResponse(
                {"deleted": count > 0}
            )
        except Customer.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid customer ID"},
                status=400,
            )

@require_http_methods(['GET', 'POST'])
def api_list_sales(request):
    """
    Lists all sales.

    Returns a dictionary with key "sales" whose value is
    a list. Each item in the list is a dictionary that
    contains the keys "salesperson", "automobile", "customer",
    and "price" for that sale. The values for salesperson,
    automobile, and customer are dictionaries containing those
    details.

    {
        "sales": [
            {
                "id",
                "salesperson": {
                    "last_name": last name,
                    "first_name": first name,
                    "employee_id": employee ID
                },
                "automobile": {
                    "vin": VIN,
                    "sold": true or false Boolean value,
                    "id": automobileVO object ID
                },
                "customer": {
                    "last_name": last name,
                    "first_name: first name
                },
                "price": price
            }
        ]
    }
    """

    if request.method == "GET":
        try:
            sales = Sale.objects.all()
            return JsonResponse(
                {"sales": sales},
                encoder=SaleEncoder,
                )
        except Sale.DoesNotExist:
            return JsonResponse({
                "message": "Something went wrong"
            }, status=400)

    else:  # POST
        content = json.loads(request.body)
        customer_id = content["customer"]
        salesperson_id = content["salesperson"]
        vin = content["vin"]
        print(content)

        try:
            customer = Customer.objects.get(id=customer_id)
            salesperson = Salesperson.objects.get(id=salesperson_id)
            automobile = AutomobileVO.objects.get(vin=vin)

            content["customer"] = customer
            content["salesperson"] = salesperson
            content["automobile"] = automobile
            print(content["automobile"])
            del content["vin"]

        except (
                Customer.DoesNotExist,
                Salesperson.DoesNotExist,
                AutomobileVO.DoesNotExist):
            return JsonResponse(
                {"message": "Invalid customer ID, salesperson ID, or automobile VIN"},
                status=400,
            )

        sale = Sale.create(**content)
        return JsonResponse(
            sale,
            encoder=SaleEncoder,
            safe=False,
        )


@require_http_methods(['DELETE'])
def api_show_sale(request, id):
    """
    Deletes a sale, identified by its object id.
    """

    if request.method == 'DELETE':
        try:
            count, _ = Sale.objects.get(id=id).delete()
            return JsonResponse(
                {"deleted": count > 0}
            )
        except Sale.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid sale ID"},
                status=400
            )
