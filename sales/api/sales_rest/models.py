from django.db import models
from django.urls import reverse


class AutomobileVO(models.Model):
    vin = models.CharField(max_length=17, unique=True)
    sold = models.BooleanField(default=False)


class Salesperson(models.Model):
    first_name = models.CharField(max_length=200)
    last_name = models.CharField(max_length=200)
    employee_id = models.CharField(max_length=200, unique=True)

    def __str__(self):
        return f"{self.last_name}, {self.first_name}"

    def get_api_url(self):
        return reverse("show_salesperson", kwargs={"pk": self.pk})

    class Meta:
        ordering = ("last_name", "first_name")

    @classmethod
    def create(cls, **kwargs):
        salesperson = cls(**kwargs)
        salesperson.save()
        return salesperson


class Customer(models.Model):
    first_name = models.CharField(max_length=200)
    last_name = models.CharField(max_length=200)
    address = models.CharField(max_length=200)
    phone_number = models.CharField(max_length=25)

    def __str__(self):
        return f"{self.last_name}, {self.first_name}"

    def get_api_url(self):
        return reverse("show_customer", kwargs={"pk": self.pk})

    class Meta:
        ordering = ("last_name", "first_name")

    @classmethod
    def create(cls, **kwargs):
        customer = cls(**kwargs)
        customer.save()
        return customer


class Sale(models.Model):
    automobile = models.ForeignKey(
        AutomobileVO,
        related_name="sale",
        on_delete=models.PROTECT,
        null=True,
        blank=True,
    )
    salesperson = models.ForeignKey(
        Salesperson,
        related_name="sale",
        on_delete=models.PROTECT,
        null=True,
        blank=True,
    )
    customer = models.ForeignKey(
        Customer,
        related_name="sale",
        on_delete=models.PROTECT,
        null=True,
        blank=True,
    )
    price = models.PositiveIntegerField()

    def get_api_url(self):
        return reverse("show_sale", kwargs={"pk": self.pk})

    @classmethod
    def create(cls, **kwargs):
        sale = cls(**kwargs)
        sale.save()
        return sale

    class Meta:
        ordering = ("automobile", "price", "customer", "salesperson")
