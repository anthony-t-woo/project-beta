from common.json import ModelEncoder

from .models import Salesperson, Customer, AutomobileVO, Sale


class SalespersonListEncoder(ModelEncoder):
    model = Salesperson
    properties = ["last_name", "first_name", "employee_id", "id"]



class CustomerListEncoder(ModelEncoder):
    model = Customer
    properties = ["last_name", "first_name", "address", "phone_number", "id"]


class AutomobileVOEncoder(ModelEncoder):
    model = AutomobileVO
    properties = ["vin", "sold", "id"]


class SaleEncoder(ModelEncoder):
    model = Sale
    properties = ["id", "salesperson", "automobile", "customer", "price"]

    encoders = {
        "salesperson": SalespersonListEncoder(),
        "automobile": AutomobileVOEncoder(),
        "customer": CustomerListEncoder(),
    }
