import React, { useState } from "react";

export default function SalespersonForm({ loadSalespeople }) {
  const [isSubmitted, setIsSubmitted] = useState(false);
  const [first_name, setFirstName] = useState("");
  const [last_name, setLastName] = useState("");
  const [employee_id, setEmployeeID] = useState("");

  const handleFirstNameChange = (e) => {
    const value = e.target.value;
    setFirstName(value);
  };

  const handleLastNameChange = (e) => {
    const value = e.target.value;
    setLastName(value);
  };

  const handleEmployeeIDChange = (e) => {
    const value = e.target.value;
    setEmployeeID(value);
  };

  const handleSubmit = async (e) => {
    e.preventDefault();
    const data = {
      first_name: first_name,
      last_name: last_name,
      employee_id: employee_id,
    };

    const newSalespersonURL = "http://localhost:8090/api/salespeople/";
    const fetchConfig = {
      method: "post",
      body: JSON.stringify(data),
      headers: {
        "Content-Type": "application/json",
      },
    };

    const response = await fetch(newSalespersonURL, fetchConfig);
    if (response.ok) {
      setIsSubmitted(true);
      loadSalespeople();
      setFirstName("");
      setLastName("");
      setEmployeeID("");
    } else {
      console.error(response);
    }
  };

  return (
    <div className="row">
      <div className="offset-3 col-6">
        <div className="shadow p-4 mt-4">
          <form onSubmit={handleSubmit} id="create-salesperson-form">
            <h1 className="card-title">Add a New Salesperson</h1>
            <div className="form-floating mb-3">
              <input
                onChange={handleFirstNameChange}
                required
                value={first_name}
                placeholder="First Name"
                type="text"
                id="first-name"
                name="first-name"
                className="form-control"
              />
              <label htmlFor="first-name">First Name</label>
            </div>
            <div className="form-floating mb-3">
              <input
                onChange={handleLastNameChange}
                required
                value={last_name}
                placeholder="Last Name"
                type="text"
                id="last-name"
                name="last-name"
                className="form-control"
              />
              <label htmlFor="last-name">Last Name</label>
            </div>
            <div className="form-floating mb-3">
              <input
                onChange={handleEmployeeIDChange}
                required
                value={employee_id}
                placeholder="e.g. jgarcia"
                type="text"
                id="employeeID"
                name="employeeID"
                className="form-control"
              />
              <label htmlFor="employeeID">Employee ID</label>
            </div>
            <button className="btn btn-primary mb-3">Create</button>

            {isSubmitted === true && (
              <div className="alert alert-success mb-0" id="success-message">
                <h3>You successfully created a new salesperson.</h3>
              </div>
            )}
          </form>
        </div>
      </div>
    </div>
  );
}
