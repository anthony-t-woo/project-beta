import React, { useState } from 'react';

export default function TechnicianForm({ loadTechnicians }) {
    const [first_name, setFirstName] = useState('');
    const [last_name, setLastName] = useState('');
    const [employee_id, setEmployeeId] = useState('');
    const [isSubmitted, setIsSubmitted] = useState(false);

    const handleFirstNameChange = (e) => {
        const value = e.target.value;
        setFirstName(value);
    };
    const handleLastNameChange = (e) => {
        const value = e.target.value;
        setLastName(value);
    };
    const handleEmployeeIdChange = (e) => {
        const value = e.target.value;
        setEmployeeId(value);
    };

    const handleSubmit = async (e) => {
        e.preventDefault();
        const data = { first_name, last_name, employee_id };
        const fetchOptions = {
            method: 'post',
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json',
            },
        };
        const url = 'http://localhost:8080/api/technicians/';
        const response = await fetch(url, fetchOptions);
        if (response.ok) {
            loadTechnicians();
            setIsSubmitted(true);
            setFirstName('');
            setLastName('');
            setEmployeeId('');
        } else {
            console.error(response);
        }
    };
    return (
        <div className="row">
            <div className="offset-3 col-6">
                <div className="shadow p-4 mt-4">
                    <h1>Add a New Technician</h1>
                    <form onSubmit={handleSubmit}>
                        <div className="form-floating mb-3">
                            <input
                                className="form-control"
                                onChange={handleFirstNameChange}
                                placeholder="First name"
                                required
                                name="first_name"
                                value={first_name}
                                type="text"
                                id="first_name"
                            />
                            <label htmlFor="first_name">First Name</label>
                        </div>
                        <div className="form-floating mb-3">
                            <input
                                className="form-control"
                                onChange={handleLastNameChange}
                                placeholder="Last name"
                                required
                                name="last_name"
                                value={last_name}
                                type="text"
                                id="last_name"
                            />
                            <label htmlFor="last_name">Last Name</label>
                        </div>
                        <div className="form-floating mb-3">
                            <input
                                className="form-control"
                                onChange={handleEmployeeIdChange}
                                placeholder="Employee ID"
                                required
                                name="employee_id"
                                value={employee_id}
                                type="text"
                                id="employee_id"
                            />
                            <label htmlFor="employee_id">Employee ID</label>
                        </div>
                        <button className="btn btn-primary mb-3">Create</button>
                        {isSubmitted === true && (
                            <div
                                className="alert alert-success mb-0"
                                id="success-message"
                            >
                                <h3>
                                    You successfully registered a new
                                    technician.
                                </h3>
                            </div>
                        )}
                    </form>
                </div>
            </div>
        </div>
    );
}
