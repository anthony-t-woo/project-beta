import React, { useState } from "react";

export default function ManufacturerForm({ loadManufacturers }) {
  const [manufacturer, setManufacturer] = useState("");
  const [isSubmitted, setIsSubmitted] = useState(false);

  const handleManufacturerChange = (e) => {
    const value = e.target.value;
    setManufacturer(value);
  };

  const handleSubmit = async (e) => {
    e.preventDefault();
    const data = { name: manufacturer };
    const fetchOptions = {
      method: "post",
      body: JSON.stringify(data),
      headers: {
        "Content-Type": "application/json",
      },
    };
    const url = "http://localhost:8100/api/manufacturers/";
    const response = await fetch(url, fetchOptions);
    if (response.ok) {
      loadManufacturers();
      setIsSubmitted(true);
      setManufacturer("");
    } else {
      console.error(response);
    }
  };

  return (
    <div className="row">
      <div className="offset-3 col-6">
        <div className="shadow p-4 mt-4">
          <h1>Add a New Manufacturer</h1>
          <form onSubmit={handleSubmit}>
            <div className="form-floating mb-3">
              <input
                className="form-control"
                onChange={handleManufacturerChange}
                placeholder="Manufacturer name"
                required
                name="manufacturer"
                value={manufacturer}
                type="text"
                id="manufacturer"
              />
              <label htmlFor="manufacturer">Manufacturer Name</label>
            </div>
            <button className="btn btn-primary mb-3">Create</button>
            {isSubmitted === true && (
              <div className="alert alert-success mb-0" id="success-message">
                <h3>You successfully registered a new manufacturer.</h3>
              </div>
            )}
          </form>
        </div>
      </div>
    </div>
  );
}
