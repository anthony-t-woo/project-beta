import React, { useState } from "react";
import { useNavigate } from "react-router-dom";

export default function AppointmentForm({
  loadAppointments,
  technicians,
  customers,
}) {
  const [vin, setVin] = useState("");
  const [customer, setCustomer] = useState("");
  const [date, setDate] = useState("");
  const [time, setTime] = useState("");
  const [technician, setTechnician] = useState("");
  const [reason, setReason] = useState("");
  const [isSubmitted, setIsSubmitted] = useState(false);

  const handleVinChange = (e) => {
    const value = e.target.value;
    setVin(value);
  };
  const handleCustomerChange = (e) => {
    const value = e.target.value;
    setCustomer(value);
  };
  const handleDateChange = (e) => {
    const value = e.target.value;
    setDate(value);
  };
  const handleTimeChange = (e) => {
    const value = e.target.value;
    setTime(value);
  };
  const handleTechnicianChange = (e) => {
    const value = e.target.value;
    setTechnician(value);
  };
  const handleReasonChange = (e) => {
    const value = e.target.value;
    setReason(value);
  };

  const navigate = useNavigate();
  const navigateToCustomersForm = () => {
    navigate("/customers/new/");
  };

  const handleSubmit = async (e) => {
    e.preventDefault();
    const data = {
      vin: vin,
      customer: customer,
      date_time: `${date} ${time}`,
      technician: technician,
      reason: reason,
    };
    const fetchOptions = {
      method: "post",
      body: JSON.stringify(data),
      headers: { "Content-Type": "application/json" },
    };
    const url = "http://localhost:8080/api/appointments/";
    const response = await fetch(url, fetchOptions);
    if (response.ok) {
      setVin("");
      setCustomer("");
      setDate("");
      setTime("");
      setTechnician("");
      setReason("");
      setIsSubmitted(true);
      loadAppointments();
    }
  };

  return (
    <div className="row">
      <div className="offset-3 col-6">
        <div className="shadow p-4 mt-4">
          <h1>Create a Service Appointment</h1>
          <form onSubmit={handleSubmit}>
            <div className="form-floating mb-3">
              <input
                onChange={handleVinChange}
                className="form-control"
                placeholder="Automobile VIN"
                required
                name="vin"
                value={vin}
                type="text"
                id="vin"
              />
              <label htmlFor="vin">Automobile VIN</label>
            </div>
            <div className="form-floating mb-3">
              <div
                id="customerSelectHelp"
                onClick={navigateToCustomersForm}
                className=" btn btn-outline-secondary "
              >
                Add Customer
              </div>
              <select
                onChange={handleCustomerChange}
                name="customer"
                id="customer"
                value={customer}
                className="form-select"
                required
              >
                <option value="">Customer</option>
                {customers.map((customer) => {
                  return (
                    <option
                      value={customer.first_name + " " + customer.last_name}
                      key={customer.id}
                    >
                      {customer.last_name}, {customer.first_name}
                    </option>
                  );
                })}
              </select>
            </div>
            <div className="form-floating mb-3">
              <input
                onChange={handleDateChange}
                className="form-control"
                placeholder="mm/dd/yy"
                required
                name="date"
                value={date}
                type="date"
                id="date"
              />
              <label htmlFor="date">Date</label>
            </div>
            <div className="form-floating mb-3">
              <input
                onChange={handleTimeChange}
                className="form-control"
                placeholder="hh:mm"
                required
                name="time"
                value={time}
                type="time"
                id="time"
              />
              <label htmlFor="time">Time</label>
            </div>
            <div className="form-floating mb-3">
              <select
                onChange={handleTechnicianChange}
                name="technician"
                id="technician"
                className="form-select"
                required
                value={technician}
              >
                <option value="">Technicians</option>
                {technicians.map((technician) => (
                  <option value={technician.id} key={technician.id}>
                    {technician.first_name}
                  </option>
                ))}
              </select>
            </div>
            <div className="form-floating mb-3">
              <input
                onChange={handleReasonChange}
                className="form-control"
                placeholder="reason for visit"
                required
                name="reason"
                value={reason}
                type="text"
                id="reason"
              />{" "}
              <label htmlFor="reason">Reason</label>
            </div>
            <button className="btn btn-primary mb-3">Create</button>
            {isSubmitted === true && (
              <div className="alert alert-success mb-0" id="success-message">
                <h3>You successfully created a new appointment.</h3>
              </div>
            )}
          </form>
        </div>
      </div>
    </div>
  );
}
