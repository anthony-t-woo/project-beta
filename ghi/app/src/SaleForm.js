import React, { useState } from "react";

export default function SaleForm({
  autos,
  salespeople,
  customers,
  loadAutomobiles,
  loadSales,
}) {
  const [vin, setVIN] = useState("");
  const [salesperson, setSalesperson] = useState("");
  const [customer, setCustomer] = useState("");
  const [price, setPrice] = useState("");
  const [isSubmitted, setIsSubmitted] = useState(false);

  const handleVinChange = (e) => {
    const value = e.target.value;
    setVIN(value);
  };

  const handleSalespersonChange = (e) => {
    const value = e.target.value;
    setSalesperson(value);
  };

  const handleCustomerChange = (e) => {
    const value = e.target.value;
    setCustomer(value);
  };

  const handlePriceChange = (e) => {
    const value = e.target.value;
    setPrice(value);
  };

  const handleSubmit = async (e) => {
    e.preventDefault();
    const data = {
      vin: vin,
      salesperson: salesperson,
      customer: customer,
      price: price,
    };

    const newSaleURL = "http://localhost:8090/api/sales/";
    const fetchOptions = {
      method: "post",
      body: JSON.stringify(data),
      headers: {
        "Content-Type": "application/json",
      },
    };
    const putBody = { sold: true };

    const response = await fetch(newSaleURL, fetchOptions);
    if (response.ok) {
      const soldURL = `http://localhost:8100/api/automobiles/${vin}/`;
      const fetchConfig = {
        method: "put",
        body: JSON.stringify(putBody),
        headers: {
          "Content-Type": "application/json",
        },
      };

      const soldResponse = await fetch(soldURL, fetchConfig);
      if (soldResponse.ok) {
        setIsSubmitted(true);
        loadAutomobiles();
        loadSales();
        setVIN("");
        setSalesperson("");
        setCustomer("");
        setPrice("");
      } else {
        console.error(response);
      }
    } else {
      console.error(response);
    }
  };

  return (
    <div className="my-5 container">
      <div className="row">
        <div className="col">
          <div className="card shadow">
            <div className="card-body">
              <form onSubmit={handleSubmit} id="create-sale-form">
                <h1 className="card-title">Register Sale</h1>
                <div className="mb-3">
                  <select
                    onChange={handleVinChange}
                    name="vin"
                    id="vin"
                    value={vin}
                    className="form-select"
                    required
                  >
                    <option value="">Automobile VIN</option>
                    {autos
                      .filter((auto) => auto.sold === false)
                      .map((auto) => {
                        return (
                          <option value={auto.vin} key={auto.id}>
                            {auto.vin} - {auto.year}{" "}
                            {auto.model.manufacturer.name} {auto.model.name}
                          </option>
                        );
                      })}
                  </select>
                </div>
                <div className="row">
                  <div className="col">
                    <div className="form-floating mb-3">
                      <select
                        onChange={handleSalespersonChange}
                        name="salesperson"
                        id="salesperson"
                        value={salesperson}
                        className="form-select"
                        required
                      >
                        <option value="">Salesperson</option>
                        {salespeople.map((person) => {
                          return (
                            <option value={person.id} key={person.id}>
                              {person.last_name}, {person.first_name}
                            </option>
                          );
                        })}
                      </select>
                    </div>
                  </div>
                </div>
                <div className="col">
                  <div className="form-floating mb-3">
                    <select
                      onChange={handleCustomerChange}
                      name="customer"
                      id="customer"
                      value={customer}
                      className="form-select"
                      required
                    >
                      <option value="">Customer</option>
                      {customers.map((customer) => {
                        return (
                          <option value={customer.id} key={customer.id}>
                            {customer.last_name}, {customer.first_name}
                          </option>
                        );
                      })}
                    </select>
                  </div>
                </div>
                <div className="col">
                  <div className="form-floating mb-3">
                    <input
                      onChange={handlePriceChange}
                      required
                      placeholder="Price"
                      type="text"
                      id="price"
                      name="price"
                      className="form-control"
                    />
                    <label htmlFor="price">Price</label>
                  </div>
                </div>
                <button className="btn btn-lg btn-primary mb-3">
                  Register Sale
                </button>
                {isSubmitted === true && (
                  <div
                    className="alert alert-success mb-0"
                    id="success-message"
                  >
                    <h3>You successfully registered a new sale.</h3>
                  </div>
                )}
              </form>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}
